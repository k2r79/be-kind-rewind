﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using BeKindRewind.Models;

namespace BeKindRewind.Controllers
{
    public class FilmController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            ViewData["categories"] = Category.getCategories();
            ViewData["languages"] = Language.getLanguages();
            ViewData["films"] = Film.getFilms();
            ViewData["currentPage"] = "film";
            ViewData["customer"] = Session["customer"];
            
            return View();
        }

    }
}
