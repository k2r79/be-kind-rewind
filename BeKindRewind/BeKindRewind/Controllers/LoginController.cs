﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BeKindRewind.Models;

namespace BeKindRewind.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/
        public ActionResult Index()
        {
            ViewData["fail"] = Request.QueryString["fail"] != null ? Request.QueryString["fail"] : "0";

            return View();
        }

        //
        // GET: /Login/check
        public ActionResult Check()
        {
            if (string.IsNullOrEmpty(Request.Form["username"]))
            {
                return RedirectToAction("Index", new { fail = 1 });
            }

            if (string.IsNullOrEmpty(Request.Form["password"]))
            {
                return RedirectToAction("Index", new { fail = 1 });
            }

            Customer customer = new Customer(Request.Form["username"], Request.Form["password"]);

            //Process login
            if (!customer.CheckLogin()) {
                if (Session != null)
                {
                    Session.Clear();
                }

                return RedirectToAction("Index", new { fail = 1});
            }
            else {
                Session["customer"] = customer;

                return RedirectToAction("Index", new { controller = "Film", Action = "Index" });
            }
        }

        //
        // GET: /Login/logout
        public ActionResult Logout()
        {
            Session.Clear();

            return RedirectToAction("Index", new { controller = "Film", Action = "Index" });
        }
    }
}
