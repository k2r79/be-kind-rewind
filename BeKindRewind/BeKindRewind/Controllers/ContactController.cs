﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using Oracle.DataAccess.Client;

namespace BeKindRewind.Controllers
{
    public class ContactController : Controller
    {
        //
        // GET: /Contact/

        public ActionResult Index()
        {
            string connString = System.Configuration.ConfigurationManager.ConnectionStrings["oracleConnectionString"].ConnectionString;
            OracleConnection conn = new OracleConnection(connString);
            conn.Open();

            string actorQuery = "SELECT * FROM actor WHERE actor_id = 1";
            OracleCommand actorCommand = new OracleCommand(actorQuery, conn);
            actorCommand.CommandType = CommandType.Text;

            OracleDataReader reader = actorCommand.ExecuteReader();
            while (reader.Read())
            {
                ViewData["actor"] = "Prénom: " + reader.GetString(1);
            }

            conn.Close();
            conn.Dispose();

            ViewData["currentPage"] = "magasin";
            return View();
        }

    }
}
