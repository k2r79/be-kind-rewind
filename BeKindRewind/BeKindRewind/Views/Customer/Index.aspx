﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Main.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Be Kind Rewind : Profil
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="container panel panel-default">
		    <div class="col-md-6 margin-top20">
			    <div class="panel panel-default bloc-profil">
				    <div class="panel-heading">Mon profil</div>
				    <div class="panel-body">
					    <%= Html.ValidationSummary("Erreur de modification.") %>
                        <% using (Html.BeginForm("Customer", "update", FormMethod.Post, new { @class = "form-horizontal" }))
                           { %>
                           <div class="form-group">
							    <div class="col-lg-10">
								    <label for="prenom" class="control-label">Prénom</label>
								    <%= Html.TextBox("prenom", "", new { @class = "form-control", @placeholder = "Prénom..." }) %>
							        <%= Html.ValidationMessage("prenom", "Prénom incorrect.") %>
							    </div>
						    </div>
						    <div class="form-group">
							    <div class="col-lg-10">
								    <label for="nom" class="control-label">Nom</label>
								    <%= Html.TextBox("nom", "", new { @class = "form-control", @placeholder = "Nom..." }) %>
							        <%= Html.ValidationMessage("nom", "Nom incorrect.") %>
							    </div>
						    </div>
						    <div class="form-group">
							    <div class="col-lg-10">
								    <label for="email" class="control-label">Email</label>
                                    <%= Html.TextBox("email", "", new { @class = "form-control", @placeholder = "Email..." }) %>
							        <%= Html.ValidationMessage("email", "Email incorrect.") %>
                                </div>
						    </div>
						    <div class="form-group">
							    <div class="col-lg-10">
								    <label for="tel" class="control-label">Telephone</label>
								    <%= Html.TextBox("tel", "", new { @class = "form-control", @placeholder = "Téléphone..." }) %>
							        <%= Html.ValidationMessage("tel", "Téléphone incorrect.") %>
							    </div>
						    </div>

						    <div class="form-group pull-right">
							    <div class="col-lg-10" style="margin-top: 100px;">
							        <button type="submit" class="btn btn-default">Enregistrer</button>
							    </div>
						    </div>
                            <div class="clearfix"></div>
					    <% } %>
                        <div class="clearfix"></div>
				    </div>
                    <div class="clearfix"></div>
			    </div>
                <div class="clearfix"></div>
		    </div>
		    <div class="col-md-6 margin-top20">
			    <div class="panel panel-default">
				    <div class="panel-heading">Mes adresses</div>
				    <div class="panel-body">
					    <form class="form-horizontal">
                            <label for="selectAdresse" class="control-label">Adresse</label>
						    <select id="selectAdresse" class="span1 form-control">
							    <option>Maison</option>
							    <option>Boulot</option>		
							    <option>Sapin</option>		
						    </select>
						    <a href="" >Ajouter une adresse</a>
					
						    <div class="form-group margin-top20">
							    <div class="col-lg-10">
								    <label for="inputAdresse1" class="control-label">Adresse</label>
								    <input type="text" class="form-control" id="inputAdresse1" placeholder="Adresse">
								    <input type="text" class="form-control margin-top10" id="inputAdresse2" placeholder="">
							    </div>
						    </div>
						    <div class="form-group">
							    <div class="col-lg-10">
								    <label for="inputDistrict" class="control-label">District</label>
								    <input type="text" class="form-control" id="inputDistrict" placeholder="District">
							    </div>
						    </div>
						    <div class="form-group">
							    <div class="col-lg-10">
								    <label for="inputCp" class="control-label">Code Postal</label>
								    <input type="text" class="form-control" id="inputCp" placeholder="CP">
							    </div>
						    </div>
						    <div class="form-group">
							    <div class="col-lg-10">
								    <label for="inputVille" class="control-label">Ville</label>
								    <input type="email" class="form-control" id="inputVille" placeholder="Ville">
							    </div>
						    </div>

						    <div class="form-group pull-right">
							    <div class="col-lg-10">
							     <button type="submit" class="btn btn-default">Enregistrer</button>
							    </div>
						    </div>
						
					    </form>
				    </div>
			    </div>
		    </div>
	    </div>
    </div>
</asp:Content>
