﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Main.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Be Kind Rewind : Films
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="container">
        <div id="search-panel" class="panel panel-default form-horizontal">
            <div class="input-group col-md-3 col-xs-12">
                <span class="input-group-addon glyphicon glyphicon-search"></span>
                <input type="text" name="search" class="form-control" placeholder="Rechercher">
            </div>
            <div class="col-md-2 col-sm-4">
                <select class="form-control">
                    <option selected>Genre ...</option>
                    <% foreach (BeKindRewind.Models.Category category in (ArrayList)ViewData["categories"]) { %>
                        <option value="<%= category.Id %>"><%= category.Name %></option>
                    <% } %>
                </select>
            </div>
            <div class="col-md-2 col-sm-4">
                <select class="form-control">
                    <option selected>Langue ...</option>
                    <% foreach (BeKindRewind.Models.Language language in (ArrayList)ViewData["languages"]) { %>
                        <option value="<%= language.Id %>"><%= language.Name %></option>
                    <% } %>
                </select>
            </div>
            <div>
                <button type="button" class="btn btn-default pull-right" type="submit">Rechercher</button>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="panel panel-default form-horizontal">
            <% foreach (BeKindRewind.Models.Film film in (ArrayList)ViewData["films"]) { %>
                <div class="col-md-2 col-sm-4 col-xs-6">
                    <a href="#" class="thumbnail">
                        <img src="http://fakeimg.pl/800x1200/" alt="<%= film.Title %>">
                        <div class="label label-info film-label"><%= film.Title %></div>
                    </a>
                </div>
            <% } %>
            <div class="clearfix"></div>
        </div>
    </div>

</asp:Content>
