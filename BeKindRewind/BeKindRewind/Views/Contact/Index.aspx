﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Main.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Be Kind Rewind : Contact
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
		<div class="panel panel-default">
			<div class="panel-heading"><h4>Mon magasin</h4></div>
			<div class="panel-body">
				<div class="container">
					<div class="col-md-4 panel panel-default">
						<div class="margin-top20">
							<iframe style="border-radius: 5px;" width="325" height="323" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.fr/maps?f=q&amp;source=s_q&amp;hl=fr&amp;geocode=&amp;q=bordeaux&amp;sll=46.22475,2.0517&amp;sspn=26.668707,57.084961&amp;ie=UTF8&amp;hq=&amp;hnear=Bordeaux,+Gironde,+Aquitaine&amp;t=m&amp;z=12&amp;ll=44.837789,-0.57918&amp;output=embed"></iframe><br /><small><a href="https://maps.google.fr/maps?f=q&amp;source=embed&amp;hl=fr&amp;geocode=&amp;q=bordeaux&amp;sll=46.22475,2.0517&amp;sspn=26.668707,57.084961&amp;ie=UTF8&amp;hq=&amp;hnear=Bordeaux,+Gironde,+Aquitaine&amp;t=m&amp;z=12&amp;ll=44.837789,-0.57918" style="color:#0000FF;text-align:left">Agrandir le plan</a></small>
						</div>
					</div>					
					<div class="col-md-8">	
						<div class="panel panel-default">
							<div class="panel-body">
								<h4>Be Kind Rewind Bordeaux</h5>
								<br>
								<p>6 bis avenue des freres lumiere<br>
								33000 Bordeaux<br>
								05 56 74 78 34<br>
								bordeaux@be-kind-rewind.com<br>
								</p>
								<br>
								<div class="row">
									<div class="col-sm-6 col-md-4">
										<div class="img-thumbnail">
											<img data-src="holder.js/120x120" src="data:image/png;base64," class="img-thumbnail" alt="A generic square placeholder image with a white border around it, making it resemble a photograph taken with an old instant camera">
											<div class="caption">
											<h4 align="center"s>Directeur</h4>
											</div>
										</div>
									</div>
									<div class="col-sm-6 col-md-4">
										<div class="img-thumbnail">
											<img data-src="holder.js/120x120" src="data:image/png;base64," class="img-thumbnail" alt="A generic square placeholder image with a white border around it, making it resemble a photograph taken with an old instant camera">
											<div class="caption">
											<h4 align="center">Staff</h4>
											</div>
										</div>
									</div>
									<div class="col-sm-6 col-md-4">
										<div class="img-thumbnail">
											<img data-src="holder.js/120x120" src="data:image/png;base64," class="img-thumbnail" alt="A generic square placeholder image with a white border around it, making it resemble a photograph taken with an old instant camera">
											<div class="caption">
											<h4 align="center">Staff</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>						
				</div>
			</div>
		</div>
    </div>
</asp:Content>
