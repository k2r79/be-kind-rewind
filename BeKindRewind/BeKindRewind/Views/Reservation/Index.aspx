﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Main.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Be Kind Rewind : Réservations
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="container">
        <div class="panel panel-default form-horizontal">
            <div class="col-md-2 col-sm-4 col-xs-6">
                <a href="#" class="thumbnail">
                    <img src="../../Content/img/Films/forrest-gump.jpg" alt="...">
                    <div class="label label-danger film-label">à retourner</div>
                </a>
            </div>
             <div class="col-md-2 col-sm-4 col-xs-6">
                <a href="#" class="thumbnail">
                    <img src="../../Content/img/Films/john-rambo.jpg" alt="...">
                </a>
            </div>
             <div class="col-md-2 col-sm-4 col-xs-6">
                <a href="#" class="thumbnail">
                    <img src="../../Content/img/Films/the-avengers.jpg" alt="...">
                </a>
            </div>
             <div class="col-md-2 col-sm-4 col-xs-6">
                <a href="#" class="thumbnail">
                    <img src="../../Content/img/Films/the-purge.jpg" alt="...">
                </a>
            </div>
             <div class="col-md-2 col-sm-4 col-xs-6">
                <a href="#" class="thumbnail">
                    <img src="../../Content/img/Films/this-is-the-end.jpg" alt="...">
                </a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

</asp:Content>
