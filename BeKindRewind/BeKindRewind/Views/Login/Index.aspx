﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
    <head runat="server">
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	    <title>Be Kind Rewind : Login</title>
	    <!-- Bootstrap CSS -->
        <link href="../../Content/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../Content/css/style.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="container">
          <div class="row">
            <div id="logo" class="col-md-12 text-center">
	            <img class="img-responsive" style="cursor: pointer; display: inline;" src="../../Content/img/logo.png" alt="Be Kind Rewind" />
            </div>
          </div>
          <div class="row">
            <form class="form-signin panel panel-body panel-default" method="POST" action="/Login/check">
              <% if (ViewData["fail"].Equals("1"))
                 { %>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        Votre identifiant ou mot de passe est incorrect.
                    </div>
              <% } %>
              <div class="input-group">
                <span class="input-group-addon glyphicon glyphicon-user"></span>
                <input type="text" name="username" class="form-control" placeholder="Email ..." autofocus required>
              </div>
              <br>
              <div class="input-group">
                <span class="input-group-addon glyphicon glyphicon-lock"></span>
                <input type="password" name="password" class="form-control" placeholder="Mot de Passe ..." required>
              </div>
              <a id="password-forgotten" href="#">Mot de passe oublié</a>
              <br>
              <button class="btn btn-primary pull-right" type="submit">Se Connecter</button>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
        <script src="../../Content/js/jquery.min.js" type="text/javascript"></script>
        <script src="../../Content/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
