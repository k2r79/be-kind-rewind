﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Oracle.DataAccess.Client;
using System.Collections;

namespace BeKindRewind.Models
{
    public class Language
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime last_update { get; set; }

        public static ArrayList getLanguages()
        {
            string connString = System.Configuration.ConfigurationManager.ConnectionStrings["oracleConnectionString"].ConnectionString;
            OracleConnection conn = new OracleConnection(connString);
            conn.Open();

            string langQuery = "SELECT language_id, name FROM language ORDER BY name";
            OracleCommand langCommand = new OracleCommand(langQuery, conn);
            langCommand.CommandType = CommandType.Text;

            ArrayList languages = new ArrayList();
            OracleDataReader langReader = langCommand.ExecuteReader();
            while (langReader.Read())
            {
                Language language = new Language();
                language.Id = langReader.GetInt16(0);
                language.Name = langReader.GetString(1);
                languages.Add(language);
            }

            conn.Close();
            conn.Dispose();

            return languages;
        }
    }
}