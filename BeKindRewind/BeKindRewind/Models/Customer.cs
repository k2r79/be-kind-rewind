﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Oracle.DataAccess.Client;
using System.Collections;
using System.Security.Cryptography;
using System.Text;

namespace BeKindRewind.Models
{
    public class Customer
    {
        public int Id { get; set; }
        // public Store Store { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        //public Address Adress { get; set; }
        public Boolean Active { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime lastUpdate { get; set; }
        public string Password { get; set; }

        public Customer(string username, string password)
        {
            this.Email = username;
            this.Password = EncodePassword(password);
        }

        public bool CheckLogin()
        {
            string connString = System.Configuration.ConfigurationManager.ConnectionStrings["oracleConnectionString"].ConnectionString;
            OracleConnection conn = new OracleConnection(connString);
            conn.Open();

            string loginQuery = "SELECT * " +
                                "FROM customer " +
                                "WHERE LOWER(email) = LOWER('" + this.Email + "') " +
                                    "AND LOWER(password) = LOWER('" + this.Password + "')";
            OracleCommand loginCommand = new OracleCommand(loginQuery, conn);
            loginCommand.CommandType = CommandType.Text;

            OracleDataReader loginReader = loginCommand.ExecuteReader();
            Boolean loggedIn = true;

            if (loginReader == null || !loginReader.HasRows)
            {
                loggedIn = false;
            }
            else
            {
                while (loginReader.Read())
                {
                    this.Firstname = loginReader.GetString(2);
                    this.Lastname = loginReader.GetString(3);
                }
            }

            conn.Close();
            conn.Dispose();

            return loggedIn;
        }

        public static string EncodePassword(string text)
        {
            byte[] buffer = Encoding.Default.GetBytes(text);

            SHA1CryptoServiceProvider cryptoTransformSha1 = new SHA1CryptoServiceProvider();
            string hash = BitConverter.ToString(cryptoTransformSha1.ComputeHash(buffer)).Replace("-", "");

            return hash;
        }
    }
}