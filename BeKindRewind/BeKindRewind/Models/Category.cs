﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Oracle.DataAccess.Client;
using System.Collections;

namespace BeKindRewind.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime last_update { get; set; }

        public static ArrayList getCategories()
        {
            string connString = System.Configuration.ConfigurationManager.ConnectionStrings["oracleConnectionString"].ConnectionString;
            OracleConnection conn = new OracleConnection(connString);
            conn.Open();

            string catQuery = "SELECT category_id, name FROM category ORDER BY name";
            OracleCommand catCommand = new OracleCommand(catQuery, conn);
            catCommand.CommandType = CommandType.Text;

            ArrayList categories = new ArrayList();
            OracleDataReader catReader = catCommand.ExecuteReader();
            while (catReader.Read())
            {
                Category category = new Category();
                category.Id = catReader.GetInt16(0);
                category.Name = catReader.GetString(1);
                categories.Add(category);
            }

            conn.Close();
            conn.Dispose();

            return categories;
        }
    }
}