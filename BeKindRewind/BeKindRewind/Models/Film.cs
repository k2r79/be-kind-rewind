﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Oracle.DataAccess.Client;
using System.Collections;

namespace BeKindRewind.Models
{
    public class Film
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ReleaseYear { get; set; }
        public BeKindRewind.Models.Language Language { get; set; }
        public BeKindRewind.Models.Language OriginalLanguage { get; set; }
        public int RentalDuration { get; set; }
        public decimal RentalRate { get; set; }
        public decimal Length { get; set; }
        public decimal ReplacementCost { get; set; }
        public string Rating { get; set; }
        public string SpecialFeatures { get; set; }
        public DateTime LastUpdate { get; set; }

        public static ArrayList getFilms(int limit = 24, int start = 0)
        {
            string connString = System.Configuration.ConfigurationManager.ConnectionStrings["oracleConnectionString"].ConnectionString;
            OracleConnection conn = new OracleConnection(connString);
            conn.Open();

            string filmQuery = "SELECT * " +
                               "FROM film " +
                               "WHERE ROWNUM >= " + start + " " +
                                   "AND ROWNUM <= " + (start + limit) + " " +
                               "ORDER BY last_update DESC";
            OracleCommand filmCommand = new OracleCommand(filmQuery, conn);
            filmCommand.CommandType = CommandType.Text;

            ArrayList films = new ArrayList();
            OracleDataReader filmReader = filmCommand.ExecuteReader();
            while (filmReader.Read())
            {
                Film film = new Film();
                film.Id = filmReader.GetInt16(0);
                film.Title = filmReader.GetString(1);
                film.Description = filmReader.GetString(2);
                film.ReleaseYear = filmReader.GetString(3);
                // film.Language = filmReader.GetInt16(4);
                // film.OriginalLanguage = filmReader.GetInt16(5);
                film.RentalDuration = filmReader.GetInt16(6);
                film.RentalRate = filmReader.GetDecimal(7);
                film.Length = filmReader.GetDecimal(8);
                film.ReplacementCost = filmReader.GetDecimal(9);
                film.Rating = filmReader.GetString(10);
                film.SpecialFeatures = filmReader.GetString(11);
                film.LastUpdate = filmReader.GetDateTime(12);

                films.Add(film);
            }

            conn.Close();
            conn.Dispose();

            return films;
        }
    }
}